libfile-chmod-perl (0.42-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libfile-chmod-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 16:53:42 +0000

libfile-chmod-perl (0.42-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Alejandro Garrido Mota from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 22:04:40 +0100

libfile-chmod-perl (0.42-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 13 Jun 2022 17:53:04 +0200

libfile-chmod-perl (0.42-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.42
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl
  * Update long description
  * Update year of upstream copyright

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 21 Aug 2015 21:18:29 -0300

libfile-chmod-perl (0.40-1) unstable; urgency=medium

  * New upstream release.
  * Install new CONTRIBUTING file.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2013 21:43:17 +0100

libfile-chmod-perl (0.39-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Alejandro Garrido Mota ]
  * New upstream version 0.38.
  * d/compat: Update to compatibility level 9.
  * d/control:
     - Update my email address in Uploaders field.
     - Update Standards-Version to 3.9.4.
  * d/copyright: Update file structure and fix the refers to symlink license
    (GPL)
  * Switch to dpkg-source 3.0 (quilt) format.


  [ gregor herrmann ]
  * New upstream release 0.39.
  * Update debian/copyright. New upstream contact and copyright holder;
    update years of packaging copyright.
  * Add debian/NEWS, mentioning a deprecation warning.

 -- gregor herrmann <gregoa@debian.org>  Tue, 01 Oct 2013 19:33:51 +0200

libfile-chmod-perl (0.32-1) unstable; urgency=low

  * Initial Release (Closes: #460056).

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Sep 2008 21:43:39 +0200
